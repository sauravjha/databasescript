SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema ip_health
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ip_health` DEFAULT CHARACTER SET latin1 ;
USE `ip_health` ;

-- -----------------------------------------------------
-- Table `ip_health`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`user` ;


CREATE TABLE IF NOT EXISTS `ip_health`.`user` (
  `user_id` INT(10) NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(45) NOT NULL,
  `user_password` VARCHAR(45) NOT NULL,
  `user_forms` TINYINT(1) NULL DEFAULT '0',
  `user_messaging` TINYINT(1) NULL DEFAULT '0',
  `user_notification` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`hospital`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`hospital` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`hospital` (
  `hospital_id` INT(10) NOT NULL,
  `hospital_name` VARCHAR(45) NOT NULL,
  `hospital_address_line_1` VARCHAR(60) NOT NULL,
  `hospital_address_line_2` VARCHAR(60) NULL DEFAULT NULL,
  `hospital_suburb` VARCHAR(25) NOT NULL,
  `hospital_post_code` INT(10) NOT NULL,
  `hospital_phone` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`hospital_id`),
  UNIQUE INDEX `hospital_id_UNIQUE` (`hospital_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`adminstrator`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`adminstrator` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`adminstrator` (
  `adminstrator_id` INT(10) NOT NULL,
  `adminstrator_firstname` VARCHAR(45) NOT NULL,
  `adminstrator_lastname` VARCHAR(45) NULL DEFAULT NULL,
  `adminstrator_email` VARCHAR(100) NOT NULL,
  `adminstrator_phone` VARCHAR(20) NOT NULL,
  `fk_hospitalid_id` INT(10) NOT NULL,
  PRIMARY KEY (`adminstrator_id`),
  UNIQUE INDEX `adminstrator_email_UNIQUE` (`adminstrator_email` ASC),
  UNIQUE INDEX `adminstrator_phone_UNIQUE` (`adminstrator_phone` ASC),
  INDEX `fk_hospital_id` (`fk_hospitalid_id` ASC),
  CONSTRAINT `adminstrator_id`
    FOREIGN KEY (`adminstrator_id`)
    REFERENCES `ip_health`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_hospital_id`
    FOREIGN KEY (`fk_hospitalid_id`)
    REFERENCES `ip_health`.`hospital` (`hospital_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`form_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`form_detail` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`form_detail` (
  `form_id` INT(10) NOT NULL AUTO_INCREMENT,
  `form_title` VARCHAR(45) NOT NULL,
  `form_random_question` TINYINT(1) NULL DEFAULT NULL,
  `form_setlive` TINYINT(1) NULL DEFAULT NULL,
  `form_due_date` DATE NULL DEFAULT NULL,
  `form_alert_date` DATE NULL DEFAULT NULL,
  `form_attempts` INT(10) NULL DEFAULT NULL,
  `form_email` VARCHAR(45) NULL DEFAULT NULL,
  `form_repeat` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`form_id`),
  UNIQUE INDEX `form_title_UNIQUE` (`form_title` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`form_question`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`form_question` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`form_question` (
  `form_question_id` INT(10) NOT NULL AUTO_INCREMENT,
  `form_id` INT(10) NOT NULL,
  `question` VARCHAR(100) NOT NULL,
  `question_type` VARCHAR(45) NOT NULL,
  `is_madotary` TINYINT(1) NOT NULL,
  `options` VARCHAR(100) NULL DEFAULT NULL,
  `random_orderOption` TINYINT(1) NULL DEFAULT NULL,
  `correct_answer` VARCHAR(45) NULL DEFAULT NULL,
  `Attachment_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_path` VARCHAR(100) NULL DEFAULT NULL,
  `alert_value` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`form_question_id`),
  INDEX `form_id_idx` (`form_id` ASC),
  CONSTRAINT `form_id`
    FOREIGN KEY (`form_id`)
    REFERENCES `ip_health`.`form_detail` (`form_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`alert_report`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`alert_report` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`alert_report` (
  `alert_id` INT(10) NOT NULL,
  `question_id` INT(10) NOT NULL,
  `threshold_value` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`alert_id`, `question_id`),
  INDEX `question_id_idx` (`question_id` ASC),
  CONSTRAINT `question_id`
    FOREIGN KEY (`question_id`)
    REFERENCES `ip_health`.`form_question` (`form_question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`module`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`module` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`module` (
  `module_id` INT(10) NOT NULL,
  `module_name` VARCHAR(45) NOT NULL,
  `module_discription` VARCHAR(200) NULL DEFAULT NULL,
  `module_enable` TINYINT(1) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE INDEX `module_name_UNIQUE` (`module_name` ASC),
  UNIQUE INDEX `module_discription_UNIQUE` (`module_discription` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`feature`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`feature` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`feature` (
  `feature_id` INT(10) NOT NULL,
  `feature_name` VARCHAR(25) NOT NULL,
  `feature_enable` TINYINT(1) NOT NULL,
  `fk_module_id` INT(10) NOT NULL,
  PRIMARY KEY (`feature_id`),
  INDEX `fk_module_id_idx` (`fk_module_id` ASC),
  CONSTRAINT `fk_module_id`
    FOREIGN KEY (`fk_module_id`)
    REFERENCES `ip_health`.`module` (`module_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`nurse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`nurse` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`nurse` (
  `nurse_id` INT(10) NOT NULL,
  `nurse_firstname` VARCHAR(45) NOT NULL,
  `nurse_lastname` VARCHAR(45) NOT NULL,
  `nurse_phone` VARCHAR(20) NOT NULL,
  `nurse_email` VARCHAR(100) NOT NULL,
  `fk_admistrator_id` INT(10) NOT NULL,
  PRIMARY KEY (`nurse_id`),
  UNIQUE INDEX `nurse_email_UNIQUE` (`nurse_email` ASC),
  UNIQUE INDEX `nurse_phone_UNIQUE` (`nurse_phone` ASC),
  UNIQUE INDEX `nurse_id_UNIQUE` (`nurse_id` ASC),
  INDEX `adminstrator_id_idx` (`fk_admistrator_id` ASC),
  CONSTRAINT `fk_adminstrator_id`
    FOREIGN KEY (`fk_admistrator_id`)
    REFERENCES `ip_health`.`adminstrator` (`adminstrator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `nurse_id`
    FOREIGN KEY (`nurse_id`)
    REFERENCES `ip_health`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`patient_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`patient_group` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`patient_group` (
  `group_id` INT(10) NOT NULL,
  `group_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`patient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`patient` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`patient` (
  `patient_id` INT(10) NOT NULL,
  `patient_firstname` VARCHAR(45) NOT NULL,
  `patient_lastname` VARCHAR(45) NOT NULL,
  `patient_phone` VARCHAR(20) NOT NULL,
  `patient_email` VARCHAR(100) NOT NULL,
  `patient_address_line_1` VARCHAR(60) NOT NULL,
  `patient_address_line_2` VARCHAR(60) NULL DEFAULT NULL,
  `patient_suburb` VARCHAR(25) NOT NULL,
  `patient_postcode` INT(10) NULL DEFAULT NULL,
  `nurse_user_id` INT(10) NOT NULL,
  `patient_group_id` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`patient_id`),
  UNIQUE INDEX `patient_email_UNIQUE` (`patient_email` ASC),
  UNIQUE INDEX `patient_phone_UNIQUE` (`patient_phone` ASC),
  INDEX `nurse_user_id_idx` (`nurse_user_id` ASC),
  INDEX `patient_group_id_idx` (`patient_group_id` ASC),
  CONSTRAINT `nurse_user_id`
    FOREIGN KEY (`nurse_user_id`)
    REFERENCES `ip_health`.`nurse` (`nurse_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `patient_group_id`
    FOREIGN KEY (`patient_group_id`)
    REFERENCES `ip_health`.`patient_group` (`group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `patient_id`
    FOREIGN KEY (`patient_id`)
    REFERENCES `ip_health`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`form_nurse_patient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`form_nurse_patient` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`form_nurse_patient` (
  `form_patient_id` INT(10) NOT NULL AUTO_INCREMENT,
  `fk_form_id` INT(10) NOT NULL,
  `fk_patient_id` INT(10) NOT NULL,
  PRIMARY KEY (`form_patient_id`),
  INDEX `fk_patient_id_idx` (`fk_patient_id` ASC),
  INDEX `fk_form_id` (`fk_form_id` ASC),
  CONSTRAINT `fk_form_id`
    FOREIGN KEY (`fk_form_id`)
    REFERENCES `ip_health`.`form_detail` (`form_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_patient_id`
    FOREIGN KEY (`fk_patient_id`)
    REFERENCES `ip_health`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`report_answer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`report_answer` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`report_answer` (
  `report_answer_id` INT(10) NOT NULL,
  `fk_form_form_id` INT NOT NULL,
  `fk_form_patient_id` INT(10) NOT NULL,
  `fk_form_question_id` INT(10) NOT NULL,
  `report_answer` VARCHAR(100) NOT NULL,
  `start_time` TIME NULL,
  `end_time` TIME NULL,
  `history_id` INT NOT NULL,
  PRIMARY KEY (`report_answer_id`),
  INDEX `form_question_id_idx` (`fk_form_question_id` ASC),
  INDEX `fk_form_patient_id_idx` (`fk_form_patient_id` ASC),
  INDEX `fk_form_form_id_idx` (`fk_form_form_id` ASC),
  CONSTRAINT `fk_form_patient_id`
    FOREIGN KEY (`fk_form_patient_id`)
    REFERENCES `ip_health`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_form_question_id`
    FOREIGN KEY (`fk_form_question_id`)
    REFERENCES `ip_health`.`form_question` (`form_question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_form_form_id`
    FOREIGN KEY (`fk_form_form_id`)
    REFERENCES `ip_health`.`form_detail` (`form_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`role` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`role` (
  `role_id` INT(10) NOT NULL,
  `role_name` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`role_permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`role_permission` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`role_permission` (
  `fk_feature_id` INT(10) NOT NULL,
  `fk_rp_role_id` INT(10) NOT NULL,
  `feature_role_enable` TINYINT(1) NOT NULL,
  PRIMARY KEY (`fk_feature_id`, `fk_rp_role_id`),
  INDEX `fk_role_id_idx` (`fk_rp_role_id` ASC),
  CONSTRAINT `fk_feature_id`
    FOREIGN KEY (`fk_feature_id`)
    REFERENCES `ip_health`.`feature` (`feature_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rp_role_id`
    FOREIGN KEY (`fk_rp_role_id`)
    REFERENCES `ip_health`.`role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ip_health`.`user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ip_health`.`user_role` ;

CREATE TABLE IF NOT EXISTS `ip_health`.`user_role` (
  `fk_user_id` INT(10) NOT NULL,
  `fk_role_id` INT(10) NOT NULL,
  PRIMARY KEY (`fk_user_id`, `fk_role_id`),
  INDEX `fk_role_id_idx` (`fk_role_id` ASC),
  CONSTRAINT `fk_role_id`
    FOREIGN KEY (`fk_role_id`)
    REFERENCES `ip_health`.`role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_id`
    FOREIGN KEY (`fk_user_id`)
    REFERENCES `ip_health`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

USE `ip_health` ;

-- -----------------------------------------------------
-- procedure sp_getAllFormDetail
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_getAllFormDetail`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getAllFormDetail`()
BEGIN
select * 
from form_detail;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_getAllGroupDetail
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_getAllGroupDetail`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getAllGroupDetail`()
BEGIN
select *
from patient_group;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_getUsersAllFeatureAndModule
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_getUsersAllFeatureAndModule`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getUsersAllFeatureAndModule`(
IN p_user_name varchar(45)
)
BEGIN
select user.user_name,user_role.fk_role_id,role_permission.feature_role_enable,feature.feature_name,module.module_name
		


from user,user_role,role_permission,feature,module
where user.user_id=user_role.fk_user_id and
	user_role.fk_role_id = role_permission.fk_rp_role_id and
	feature.feature_id=role_permission.fk_feature_id and 
	module.module_id=feature.fk_module_id 
	and user.user_name=p_user_name;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_getUsersForGroup
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_getUsersForGroup`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getUsersForGroup`(
	IN p_group_name varchar(45)

)
BEGIN
select user.user_name
from patient, user,patient_group
where patient.patient_id=user.user_id and 
patient.patient_group_id=patient_group.group_id
and patient_group.group_name=p_group_name;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_storeAdminstratorDetail
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_storeAdminstratorDetail`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_storeAdminstratorDetail`(
	IN p_adminstrator_id int(10),
	IN p_adminstrator_firstname varchar(45), 
	IN p_adminstrator_lastname varchar(45) ,
	IN p_adminstrator_email varchar(100) ,
	IN p_adminstrator_phone varchar(20) ,
	IN p_fk_hospitalid_id int(10)

)
BEGIN
INSERT INTO adminstrator  
(	adminstrator_id,
	adminstrator_firstname,
	adminstrator_lastname,
	adminstrator_email,
	adminstrator_phone,
	fk_hospitalid_id 
)
values
(
	p_adminstrator_id,
	p_adminstrator_firstname,
	p_adminstrator_lastname,
	p_adminstrator_email,
	p_adminstrator_phone,
	p_fk_hospitalid_id  
);


END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_storeFormDetail
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_storeFormDetail`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_storeFormDetail`(
	IN p_form_id INT(10),
	IN p_form_title varchar(25),
	IN p_form_random_question boolean,
	IN p_form_due_date date,
	IN p_form_alert boolean,
	IN p_form_attempts INT(10),
	IN p_form_email varchar(45),
	IN p_form_repeat varchar(45)
)
BEGIN
INSERT INTO form_detail 
(	form_id,
	form_title,
	form_random_question,
	form_due_date,
	form_alert,
	form_attempts,
	form_email,
	form_repeat
)
values
(
	p_form_id,
	p_form_title,
	p_form_random_question,
	p_form_due_date,
	p_form_alert,
	p_form_attempts,
	p_form_email,
	p_form_repeat
	
);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_storeFormDetails
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_storeFormDetails`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_storeFormDetails`(
	IN p_form_title varchar(45) ,
	IN p_form_random_question boolean,
	IN p_form_setlive boolean ,
	IN p_form_due_date date,
	IN p_form_alert_date date,
	IN p_form_attempts int(10),
	IN p_form_email varchar(45),
	IN p_form_repeat varchar(45)
	)
BEGIN
INSERT INTO form_detail 
(	form_title,
	form_random_question,
	form_setlive  ,
	form_due_date ,
	form_alert_date ,
	form_attempts ,
	form_email,
	form_repeat 
)
values
(
	p_form_title,
	p_form_random_question,
	p_form_setlive  ,
	p_form_due_date ,
	p_form_alert_date ,
	p_form_attempts ,
	p_form_email,
	p_form_repeat 
	
);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_storeFormsPatient
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_storeFormsPatient`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_storeFormsPatient`(
	IN p_user_name varchar(45) ,
	IN p_form_title  varchar(45)
)
BEGIN
DECLARE p_form_id INT;
DECLARE p_user_id INT;	
	SELECT form_id FROM form_detail
	where form_title=p_form_title INTO p_form_id;
	SELECT user_id FROM user
	where user_name =p_user_name INTO p_user_id;
INSERT INTO form_nurse_patient
(	
	fk_form_id,
	fk_patient_id
)
values
(
	p_form_id,
	p_user_id
	
);


END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_storeNurseDetail
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_storeNurseDetail`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_storeNurseDetail`(
	IN p_nurse_id int(10) ,
	IN p_nurse_firstname varchar(45) ,
	IN p_nurse_lastname varchar(45) ,
	IN p_nurse_phone varchar(20) ,
	IN p_nurse_email varchar(100) ,
	IN p_fk_admistrator_id int(10)
)
BEGIN
INSERT INTO nurse
(	nurse_id,
	nurse_firstname,
	nurse_lastname,
	nurse_phone,
	nurse_email,
	fk_admistrator_id
	
)
values
(
	p_nurse_id,
	p_nurse_firstname,
	p_nurse_lastname,
	p_nurse_phone,
	p_nurse_email,
	p_fk_admistrator_id
	
);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_storePatientGroup
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_storePatientGroup`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_storePatientGroup`(
	IN  p_group_id int(10), 
	IN p_group_name varchar(45)
)
BEGIN
INSERT INTO patient_group 
(	 group_id, 
	 group_name 
)
values
(
	 p_group_id, 
	 p_group_name 
);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_storeQuestionDetail
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_storeQuestionDetail`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_storeQuestionDetail`(
	IN p_form_title varchar(45) ,
	IN p_question varchar(100) ,
	IN p_question_type varchar(45) ,
	IN p_is_madotary boolean  ,
	IN p_options varchar(100) ,
	IN p_random_orderOption boolean ,
	IN p_correct_answer varchar(45) ,
	IN p_Attachment_type varchar(100) ,
	IN p_file_path varchar(100) ,
	IN p_alert_value varchar(45)
)
BEGIN
DECLARE p_form_id INT;
	
	SELECT form_id FROM form_detail
	where form_title=p_form_title INTO p_form_id;
INSERT INTO form_question
(	

	form_id, 
	question ,
	question_type ,
	is_madotary ,
	options,
	random_orderOption ,
	correct_answer ,
	Attachment_type , 
	file_path ,
	alert_value
)
values
(
	p_form_id, 
	p_question ,
	p_question_type ,
	p_is_madotary ,
	p_options,
	p_random_orderOption ,
	p_correct_answer ,
	p_Attachment_type , 
	p_file_path ,
	p_alert_value
	
);


END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_storeUserDetail
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_storeUserDetail`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_storeUserDetail`(
IN p_user_id INT(10),
IN p_user_name varchar(45),
IN p_user_password varchar(45),
IN p_user_form boolean,
IN p_user_messaging boolean,
IN p_user_notification boolean
)
BEGIN
INSERT INTO user 
(	user_id,
	user_name,
	user_password,
	user_forms,
	user_messaging,
	user_notification 
)
values
(
	p_user_id,
	p_user_name,
	p_user_password,
	p_user_form,
	p_user_messaging,
	p_user_notification 
);


END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_user_getAllFeaturesAndModules
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_user_getAllFeaturesAndModules`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_getAllFeaturesAndModules`(
	IN p_user_name varchar(10),
	OUT p_user_name_out varchar(10),
	OUT p_role_id INT(10),
	OUT p_feature_enable boolean,
	OUT p_feature_name varchar(25),
	OUT p_module_name varchar(45)
)
BEGIN
select user.user_name,user_role.fk_role_id,role_permission.feature_role_enable,feature.feature_name,module.module_name
		INTO p_user_name_out,p_role_id,p_feature_enable,p_feature_name,p_module_name


from user,user_role,role_permission,feature,module
where user.user_id=user_role.fk_user_id and
	user_role.fk_role_id = role_permission.fk_rp_role_id and
	feature.feature_id=role_permission.fk_feature_id and 
	module.module_id=feature.fk_module_id 
	and user.user_name=p_user_name;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_user_getUser
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_user_getUser`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_getUser`(
IN p_user_name varchar(10),
IN p_user_password varchar(45)
)
BEGIN
	SELECT *
	FROM user
	where user_name=p_user_name and user_password=p_user_name;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_user_getUserDetail
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_user_getUserDetail`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_getUserDetail`(
	IN p_user_name varchar(10),
	IN p_user_password varchar(45),
	OUT p_user_id INT(10),
	OUT p_user_name_out varchar(10),
	out p_user_password2 varchar(45),
	out p_user_form boolean,
	out p_user_message boolean,
	out p_user_notification boolean

)
BEGIN
	SELECT user_id ,user_name, user_password, user_forms,user_messaging,user_notification
	INTO p_user_id, p_user_name_out,p_user_password2,p_user_form ,p_user_message, p_user_notification

	FROM user
	WHERE user_name = p_user_name and
	user_password= p_user_password;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_user_getalluser
-- -----------------------------------------------------

USE `ip_health`;
DROP procedure IF EXISTS `ip_health`.`sp_user_getalluser`;

DELIMITER $$
USE `ip_health`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_getalluser`()
BEGIN
select *
from user;

END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
